/**
 * 
 */
package com.newer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

/**
 * @author Administrator
 * 
 */
public class ActivitiDemoTest{

	// 创建核心引擎对象
	ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

	/**
	 * 部署流程定义
	 */
	@Test
	public void deploymentProcessDefinition() {
		// 与流程定义和部署对象相关的service
		Deployment deployment = processEngine.getRepositoryService()
				.createDeployment()
				// 创建一个部署对象
				.name("helloworld入门程序")
				// 添加部署的名称
				.addClasspathResource("bpm/TestProcess.bpmn")// classpath的资源中加载，一次只能加载
																// 一个文件
				.deploy();// 完成部署
		System.out.println("部署ID:" + deployment.getId());
		System.out.println("部署名称：" + deployment.getName());
	}

	/**
	 * 启动流程实例
	 */
	@Test
	public void startProcessInstance() {
		// 流程定义的key
		String processDefinitionKey = "myProcessTest";

		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("user", "周江霄");
		// 于正在执行的流程实例和执行对象相关的Service
		ProcessInstance pi = processEngine.getRuntimeService()
				.startProcessInstanceByKey(processDefinitionKey, variables);
		// 使用流程定义的key启动流程实例，key对应hellworld.bpmn文件中id的属性值，使用key值启动，
		// 默认是按照最新版本的流程定义启动
		System.out.println("流程实例ID:" + pi.getId());// 流程实例ID 101
		System.out.println("流程定义ID:" + pi.getProcessDefinitionId()); // 流程定义ID
	}

	/**
	 * 查询当前人的个人任务
	 */
	@Test
	public void findMyPersonTask() {
//		String assignee = "周江霄";
		String assignee = "网打哈";
		List<Task> list = processEngine.getTaskService()
		// 与正在执行的认为管理相关的Service`
				.createTaskQuery().taskAssignee(assignee)
				// 创建任务查询对象
				.list();
		if (list != null && list.size() > 0) {
			for (Task task : list) {
				System.out.println("任务ID:" + task.getId());
				System.out.println("任务名称:" + task.getName());
				System.out.println("任务的创建时间" + task);
				System.out.println("任务的办理人:" + task.getAssignee());
				System.out.println("流程实例ID:" + task.getProcessInstanceId());
				System.out.println("执行对象ID:" + task.getExecutionId());
				System.out.println("流程定义ID:" + task.getProcessDefinitionId());
				System.out.println("#################################");
			}
		}
	}
	

	/**
	 * 完成我的任务
	 */
	@Test
	public void completeMyPersonTask() {
		// 任务Id
		String taskId = "1305";
		processEngine.getTaskService()
		// 与正在执行的认为管理相关的Service
				.complete(taskId);
		System.out.println("完成任务:任务ID:" + taskId);

	}
}
