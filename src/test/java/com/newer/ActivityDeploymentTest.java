/**
 * 
 */
package com.newer;

import java.io.InputStream;
import java.util.zip.ZipInputStream;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.repository.Deployment;
import org.junit.Test;

/**
 * @author Administrator
 * 
 */
public class ActivityDeploymentTest {

	// 创建核心引擎对象
	ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

	/**
	 * 部署流程定义 类路径从classpath
	 */
	@Test
	public void deoploymentProcessDefinition_classpath() {
		// 与流程定义和部署对象相关的service
		Deployment deployment = processEngine.getRepositoryService()
				// 创建一个部署对象
				.createDeployment()
				// 添加部署的名称
				.name("第一个流程")
				// 从classpath的资源中加载，一次只能加载一个文件
				.addClasspathResource("bpm/test_activiti.bpmn")
				// 完成部署
				.deploy();
		System.out.println("部署ID：" + deployment.getId());
		System.out.println("部署名称:" + deployment.getName());
	}

	/**
	 * 部署流程定义 类路径从流文件中
	 */
	@Test
	public void deoploymentProcessDefinition_zip() {
		InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream("bpm/TestProcess.zip");
		ZipInputStream zipInputStream = new ZipInputStream(in);
		// 与流程定义和部署对象相关的service
		Deployment deployment = processEngine.getRepositoryService()
		// 创建一个部署对象
				.createDeployment()
				// 添加部署的名称
				.name("流程定义")
				// 从classpath的资源中加载，一次只能加载一个文件
				.addZipInputStream(zipInputStream).deploy();// 完成部署
		System.out.println("部署ID：" + deployment.getId());
		System.out.println("部署名称:" + deployment.getName());
	}

}
