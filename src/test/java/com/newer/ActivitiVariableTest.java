/**
 * 
 */
package com.newer;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Test;

/**
 * @author Administrator
 * 
 */
public class ActivitiVariableTest {

	// 创建核心引擎对象
	ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

	/**
	 * 部署流程定义
	 */
	@Test
	public void deploymentProcessDefinition() {
		Deployment deployment = processEngine.getRepositoryService()// 与流程定义和部署对象相关的service
				.createDeployment()// 创建一个部署对象
				.name("流程参数学习")// 添加部署的名称
				.addClasspathResource("bpm/VariableProcess.bpmn")// classpath的资源中加载，一次只能加载
																	// 一个文件
				.deploy();// 完成部署
		System.out.println("部署ID:" + deployment.getId());// 1801
		System.out.println("部署名称：" + deployment.getName());
	}

	/**
	 * 启动流程实例
	 */
	@Test
	public void startProcessInstance() {
		// 流程定义的key
		String processDefinitionKey = "variableProcess";
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("user", "周江霄");
		// 与正在执行的流程实例和执行对象相关的service
		ProcessInstance pi = processEngine.getRuntimeService()
				// 使用流程定义的key启动流程实例，key对应processVariables文件中的id的属性值，使用key值启动，默认是按照最新版本进行启动
				.startProcessInstanceByKey(processDefinitionKey, variables);
		System.out.println("流程实例ID：" + pi.getId()); // 流程实例ID：1901
		System.out.println("流程定义ID：" + pi.getProcessDefinitionId()); // 流程定义ID：variableProcess:3:1804
		System.out.println("流程实例ID" + pi.getProcessInstanceId()); // 流程实例ID1901
	}

	/**
	 * 查询任务通过流程实例id
	 */
	@Test
	public void findTask() {
		String processInstanceId = "1901";
		List<HistoricTaskInstance> list = processEngine.getHistoryService()// 与历史数据（历史表）相关的service
				.createHistoricTaskInstanceQuery()// 创建历史任务实例查询
				.processInstanceId(processInstanceId).list();
		if (list != null && list.size() > 0) {
			for (HistoricTaskInstance hti : list) {
				System.out.println(hti.getId() + "    " + hti.getName()
						+ "    " + hti.getProcessInstanceId() + "   "
						+ hti.getStartTime() + "   " + hti.getEndTime() + "   "
						+ hti.getDurationInMillis());
				System.out.println("################################");
			}
		}
	}

	/**
	 * 设置流程变量
	 */
	@Test
	public void setVariables() {
		// 与任务相关的service,正在执行的service
		TaskService taskService = processEngine.getTaskService();
		// 任务ID
		String taskId = "1905";
		// 1.设置流程变量，使用基本数据类型
		taskService.setVariable(taskId, "请假天数", 7);// 与任务ID绑定
		taskService.setVariable(taskId, "请假日期", new Date());
		taskService.setVariableLocal(taskId, "请假原因", "回去探亲，一起吃个饭123");
		System.out.println("设置流程变量成功！");
	}

	/**
	 * 获取流程变量
	 */
	@Test
	public void getVariables() {
		// 与任务（正在执行的service）
		TaskService taskService = processEngine.getTaskService();
		// 任务Id
		String taskId = "1905";
		// 1.获取流程变量，使用基本数据类型
		Integer days = (Integer) taskService.getVariable(taskId, "请假天数");
		Date date = (Date) taskService.getVariable(taskId, "请假日期");
		String reason = (String) taskService.getVariable(taskId, "请假原因");
		System.out.println("请假天数：" + days);
		System.out.println("请假日期：" + date);
		System.out.println("请假原因：" + reason);
	}

	/**
	 * 完成我的任务
	 */
	@Test
	public void completeMyPersonTask() {
		// 任务Id
		String taskId = "1905";
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("user", "网打哈");
		processEngine.getTaskService()
		// 与正在执行的认为管理相关的Service
				.complete(taskId, variables);
		System.out.println("完成任务:任务ID:" + taskId);

	}

}
