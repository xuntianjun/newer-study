/**
 * 
 */
package com.newer;

import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

/**
 * @author Administrator
 * 
 */
public class ActivitiTaskTest {

	// 创建核心引擎对象
	ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

	/**
	 * 查询历史流程实例
	 */
	@Test
	public void findHistoryProcessInstance() {
		String processInstanceId = "1301";
		HistoricProcessInstance hpi = processEngine.getHistoryService()
				.createHistoricProcessInstanceQuery()
				.processInstanceId(processInstanceId).singleResult();
		System.out.println(hpi.getId() + "    " + hpi.getProcessDefinitionId()
				+ "   " + hpi.getStartTime() + "   "
				+ hpi.getDurationInMillis());
	}

	/**
	 * 查询当前的个人任务
	 */
	@Test
	public void findMyPersonTask() {
		String assignee = "周江霄"; // TODO
		List<Task> list = processEngine.getTaskService()// 与正在执行的任务管理相关的service
				.createTaskQuery()// 创建任务查询对象
				// 查询条件
				.taskAssignee(assignee)// 指定个人任务查询，指定办理人
				// .taskCandidateGroup("")//组任务的办理人查询
				// .processDefinitionId("")//使用流程定义ID查询
				// .processInstanceId("")//使用流程实例ID查询
				// .executionId(executionId)//使用执行对象ID查询
				/** 排序 */
				.orderByTaskCreateTime().asc()// 使用创建时间的升序排列
				// 返回结果集
				// .singleResult() //返回唯一的结果集
				// .count()//返回结果集的数量
				// .listPage(firstResult, maxResults)//分页查询
				.list();// 返回列表
		if (list != null && list.size() > 0) {
			for (Task task : list) {
				System.out.println("任务ID：" + task.getId());
				System.out.println("任务名称:" + task.getName());
				System.out.println("任务的创建时间:" + task.getCreateTime());
				System.out.println("任务的办理人:" + task.getAssignee());
				System.out.println("流程实例ID:" + task.getProcessInstanceId());
				System.out.println("执行对象ID:" + task.getExecutionId());
				System.out.println("流程定义ID:" + task.getProcessDefinitionId());
				System.out
						.println("##################################################");
			}
		}
	}

	/**
	 * 查询流程状态（判断流程正在执行，还是结束）
	 */
	@Test
	public void isProcessEnd() {
		String processInstanceId = "1501";
		// 表示正在执行的流程实例和执行对象
		ProcessInstance pi = processEngine.getRuntimeService()
				// 创建流程实例查询
				.createProcessInstanceQuery()
				// 使用流程实例ID查询
				.processInstanceId(processInstanceId).singleResult();

		if (pi == null) {
			System.out.println("流程已经结束");
		} else {
			System.out.println("流程没有结束");
		}

	}

}
